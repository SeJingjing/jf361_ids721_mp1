+++
title = "Jingjing Feng"


# The homepage contents
[extra]
lead = 'About Me'
url = "/docs/getting-started/introduction/"
url_button = "Get started"
repo_version = "GitHub v0.1.0"
repo_license = "Open-source MIT License."
repo_url = "https://gitlab.com/SeJingjing/jf361_ids721_mp1"

# Menu items
[[extra.menu.main]]
name = "Docs"
section = "docs"
url = "/docs/getting-started/introduction/"
weight = 10


[[extra.menu.main]]
name = "Publications"
section = "Publications"
url = "/blog/"
weight = 20


[[extra.list]]
title = "Undergraduate"
content = 'Information and Computing Science @ XJTLU  Computer Science @ UoL'

[[extra.list]]
title = "Master"
content = 'Electrical and Computer Engineering @ Duke University'

[[extra.list]]
title = "Research Interest"
content = 'Machine learning & Reinforcement learning'

+++
