# IDS721 Individual Project 1

## Project Introduction
The objective of the project is to design, build, and deploy a personal website using modern web development frameworks and tools. The website could be developed using Zola, and should be automatically built and deployed using a GitLab workflow upon code push. The website should be hosted on a platforms, Vercel.

## Project Description
- Website built with Zola
- GitLab workflow to build and deploy site on push
- Hosted on Vercel

## Project Setup
#### 1. Use `Zola` to create a static website.
In the terminal, run command `zola serve` and then the website will be accessible at http://127.0.0.1:1111/.
#### 2. Push the website dictionary to the GitLab.
#### 3. In GitLab, under the `Deploy`, click  `Pages` to add a `gitlab-ci.yml` file to this repo.
#### 4. Link this GitLab repo to `Vercel` for hosting this website.
<p align="center">
  <img src="vercel.png" />
</p>

## Screenshot of my website
You can access my personal website via the following link: [https://jf361-ids721-mp1-sejingjing-8b215cdbf4d5550133b98a48cb6f0b66f8b.gitlab.io](https://jf361-ids721-mp1-sejingjing-8b215cdbf4d5550133b98a48cb6f0b66f8b.gitlab.io).
### Homepage
<p align="center">
  <img src="homepage.png" />
</p>


#### About
Hobby
<p align="center">
  <img src="hobby.png" />
</p>

Academic Background
<p align="center">
  <img src="academic.png" />
</p>


#### Publications
<p align="center">
  <img src="publications.png" />
</p>

A publication
<p align="center">
  <img src="apublication.png" />
</p>
